//
//  ListCollectionViewCell.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit
class ListCollectionViewCell: UICollectionViewCell, Updatable {
    static let reuseID: String = String(describing: ListCollectionViewCell.self)
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func update(model: Any) {
        guard let model = model as? CityModel else { return }
        titleLabel.text = "\(model.country), \(model.name)"
        lonLabel.text = (String(format: " %.2f", model.coord.lon))
        latLabel.text = (String(format: " %.2f", model.coord.lat))
    }
}
