//
//  ListViewController.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit
import Combine
protocol ListViewControllerDelegate: AnyObject {
    func mapButtonDidTap(models: [CityModel], with city: CityModel?)
}
class ListViewController: SharedViewController {
    weak var delegate: ListViewControllerDelegate?
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = (UIScreen.main.bounds.width)
        layout.itemSize = CGSize(width: width, height: 120)
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .white
        view.isPrefetchingEnabled = true
        return view
    }()
    lazy var collectionViewContainer: CollectionViewContainer = {
        let container = CollectionViewContainer(collectionView: collectionView)
        return container
    }()
    lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.placeholder = "Search"
        controller.searchBar.tintColor = .white
        controller.searchBar.delegate = self
        return controller
    }()
    var cancellableStore = Set<AnyCancellable>()
    let viewModel: ListViewModel
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    init(viewModel: ListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func mapButtonDidTap(sender: UIBarButtonItem) {
        if !viewModel.isLoading {
            delegate?.mapButtonDidTap(models: viewModel.cities, with: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GlobalSettings.shared().mainColor
        addCollectionView()
        viewModel.viewDidLoad(self)
        addMapViewButton()
        addSearchController()
    }
    private func addSearchController() {
        definesPresentationContext = true
        navigationItem.searchController = searchController
    }
    private func addCollectionView() {
        view.addSubview(collectionViewContainer)
        collectionViewContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([collectionViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    private func addMapViewButton() {
        let mapButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(mapButtonDidTap(sender:)))
        navigationItem.rightBarButtonItem = mapButton
    }
}
