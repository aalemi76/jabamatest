//
//  ListViewController+Ext.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit
extension ListViewController: Viewable {
    func show(result: Result<Any, FindItError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                guard let sections = data as? [Sectionable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return
                }
                self?.configureCollectionView(sections)
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
            }
        }
    }
    func configureCollectionView(_ sections: [Sectionable]) {
        collectionViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        subscribeOnTap()
        subscribeToLoadNextPage()
    }
    private func subscribeOnTap() {
        collectionViewContainer.delegateHandler.getSelectedItem().sink { [weak self] (model) in
            guard let self = self, let model = model as? CityModel else { return }
            self.delegate?.mapButtonDidTap(models: self.viewModel.cities, with: model)
        }.store(in: &cancellableStore)
    }
    //MARK:- To load new cell on next page appearance
    private func subscribeToLoadNextPage() {
        collectionViewContainer.dataSourceHandler.getNextPage().sink { [weak self] (indexPath) in
        }.store(in: &cancellableStore)
    }
}
//MARK:- Search Functionality
extension ListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard !viewModel.isLoading, let text = searchController.searchBar.text else { return }
        viewModel.filterContentForSearchedText(text)
    }
}
extension ListViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.cancelButtonClicked()
    }
}
