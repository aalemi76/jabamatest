//
//  Coordinate.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import Foundation
struct Coordinate: Codable {
    let lon: Double
    let lat: Double
}
