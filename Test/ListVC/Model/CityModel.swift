//
//  CityModel.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import Foundation
struct CityModel: Codable {
    let country: String
    let name: String
    let id: Int
    let coord: Coordinate
    enum CodingKeys: String, CodingKey {
        case country
        case name
        case id = "_id"
        case coord
    }
}
