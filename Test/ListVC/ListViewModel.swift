//
//  ListViewModel.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import Foundation
class ListViewModel: ViewModelProviderMappable {
    private var interactor: Interactor<[CityModel]>
    weak private var view: (Viewable)?
    typealias model = [CityModel]
    private var sections = [Sectionable]()
    private (set) var isLoading: Bool = true
    private (set) var cities: [CityModel] = []
    private (set) var filteredCities: [CityModel] = []
    required init(interactor: Interactor<[CityModel]>) {
        self.interactor = interactor
    }
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        refreshItems()
    }
    private func refreshItems() {
        isLoading = true
        interactor.getModel(fileName: "cities", onSuccess: { [weak self] (model) in
            guard let self = self else { return }
            self.cities = model
            self.prepareSections(items: model)
            self.view?.show(result: .success(self.sections))
            self.isLoading = false
        }) { [weak self] (error) in
            self?.view?.show(result: .failure(error))
            self?.isLoading = false
        }
    }
    private func prepareSections(items: [CityModel]) {
        let cells = items.map({ ListCollectionCellViewModel(reuseID: ListCollectionViewCell.reuseID, cellClass: ListCollectionViewCell.self, model: $0)})
        sections = [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
    }
    func filterContentForSearchedText(_ text: String) {
        filteredCities = cities.filter({ (city: CityModel) -> Bool in
            return city.name.lowercased().hasPrefix(text.lowercased())
        })
        prepareSections(items: filteredCities)
        view?.show(result: .success(self.sections))
    }
    func cancelButtonClicked() {
        prepareSections(items: cities)
        view?.show(result: .success(self.sections))
    }
    //MARK:- To load new cell on next page appearance
    /*func prepareNewCells(items: [CityModel]) -> [Reusable] {
        let cells = items.map({ ListCollectionCellViewModel(reuseID: ListCollectionViewCell.reuseID, cellClass: ListCollectionViewCell.self, model: $0)})
        sections += [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
        return cells
    }*/
}
