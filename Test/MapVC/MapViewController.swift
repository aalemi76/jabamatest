//
//  MapViewController.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit
import Combine
import Mapbox
class MapViewController: SharedViewController {
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let width = (UIScreen.main.bounds.width) - 20
        layout.itemSize = CGSize(width: width, height: 120)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 20
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .white
        view.isPrefetchingEnabled = true
        return view
    }()
    lazy var collectionViewContainer: CollectionViewContainer = {
        let container = CollectionViewContainer(collectionView: collectionView)
        return container
    }()
    lazy var mapView: MGLMapView = {
        let map = MGLMapView()
        return map
    }()
    var cancellableStore = Set<AnyCancellable>()
    let viewModel: MapViewModel
    var defaultCity: CityModel?
    init(viewModel: MapViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addCollectionView()
        viewModel.viewDidLoad(self)
        addMapView()
    }
    private func addCollectionView() {
        view.addSubview(collectionViewContainer)
        collectionViewContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([collectionViewContainer.heightAnchor.constraint(equalToConstant: 130),
            collectionViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    private func addMapView() {
        mapView.delegate = self
        mapView.backgroundColor = .white
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
        mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        mapView.bottomAnchor.constraint(equalTo: collectionViewContainer.topAnchor)])
        guard let city = defaultCity else { return }
        reCenter(with: city)
    }
}
