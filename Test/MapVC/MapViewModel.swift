//
//  MapViewModel.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import Foundation
class MapViewModel: ViewModelProviderMappable {
    private var interactor: Interactor<[CityModel]>
    weak private var view: (Viewable)?
    typealias model = [CityModel]
    private var sections = [Sectionable]()
    var cities: [CityModel] = []
    required init(interactor: Interactor<[CityModel]>) {
        self.interactor = interactor
    }
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        refreshItems()
    }
    private func refreshItems() {
        prepareSections(items: cities)
        view?.show(result: .success(self.sections))
    }
    private func prepareSections(items: [CityModel]) {
        let cells = items.map({ ListCollectionCellViewModel(reuseID: ListCollectionViewCell.reuseID, cellClass: ListCollectionViewCell.self, model: $0)})
        sections = [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
    }
    //MARK:- To load new cell on next page appearance
    /*func prepareNewCells(items: [CityModel]) -> [Reusable] {
        let cells = items.map({ ListCollectionCellViewModel(reuseID: ListCollectionViewCell.reuseID, cellClass: ListCollectionViewCell.self, model: $0)})
        sections += [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
        return cells
    }*/
}

