//
//  MapViewController+Ext.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit
import Mapbox
extension MapViewController: Viewable {
    func show(result: Result<Any, FindItError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                guard let sections = data as? [Sectionable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return
                }
                self?.configureCollectionView(sections)
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
            }
        }
    }
    private func configureCollectionView(_ sections: [Sectionable]) {
        collectionViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        subscribeOnTap()
        subscribeToLoadNextPage()
    }
    private func subscribeOnTap() {
        collectionViewContainer.delegateHandler.getSelectedItem().sink { [weak self] (city) in
            guard let city = city as? CityModel else { return }
            self?.reCenter(with: city)
        }.store(in: &cancellableStore)
    }
    //MARK:- To load new cell on next page appearance
    private func subscribeToLoadNextPage() {
        collectionViewContainer.dataSourceHandler.getNextPage().sink { [weak self] (indexPath) in
        }.store(in: &cancellableStore)
    }
}
//MARK:- Map Functionality
extension MapViewController: MGLMapViewDelegate {
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    func reCenter(with city: CityModel) {
        let annotation = MGLPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: city.coord.lat, longitude: city.coord.lon)
        annotation.title = "\(city.country), \(city.name)"
        let lon = (String(format: " %.2f", city.coord.lon))
        let lat = (String(format: " %.2f", city.coord.lat))
        annotation.subtitle = "\(lat), \(lon)"
        mapView.addAnnotation(annotation)
        mapView.setCenter(CLLocationCoordinate2D(latitude: city.coord.lat, longitude: city.coord.lon), zoomLevel: 13, animated: false)
    }
}
