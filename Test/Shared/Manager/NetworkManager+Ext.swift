//
//  NetworkManager+Ext.swift
//  FindIt
//
//  Created by Catalina on 10/16/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Alamofire
extension NetworkManager: RequestInterceptor {
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        guard let token = UserDefaultsManager.shared.getToken() else {
            completion(.success(urlRequest))
            return
        }
        var request = urlRequest
        let bearerToken = "Bearer \(token)"
        request.setValue(bearerToken, forHTTPHeaderField: "Authorization")
        completion(.success(request))
    }
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard request.retryCount < retryLimit else {
            completion(.doNotRetry)
            return
        }
        refreshToken { (isSucceed) in
            isSucceed ? completion(.retry) : completion(.doNotRetry)
        }
    }
    func refreshToken(completion: @escaping (_ isSuccess: Bool) -> Void) {
        let credentials = UserDefaultsManager.shared.getUserCredentials()
        let parameters = ["grant_type": "client_credentials",
                          "client_id": credentials.apiKey ?? "",
                          "client_secret": credentials.secretKey ?? ""]
        authorize(parameters: parameters) { (result) in
            switch result {
            case .success(let data):
                guard let token = (try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any])?["access_token"] as? String else {
                    completion(false)
                    return
                }
                UserDefaultsManager.shared.setToken(token: token)
                completion(true)
            case .failure:
                completion(false)
            }
        }
    }
}
