//
//  LoginInteractor.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
class Interactor<Model: Codable>: Parsable {
    typealias Object = Model
    func parse(data: Data) -> Model? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let model = try? decoder.decode(Model.self, from: data)
        return model
    }
    func getModel(url: String, parameters: [String: Any]?,
                  onSuccess: @escaping (Object) -> Void,
                  onFailure: @escaping (FindItError) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            NetworkManager.shared.request(url, method: .get, parameters: parameters) { [weak self] result in
                switch result {
                case .success(let data):
                    guard let model = self?.parse(data: data) else {
                        onFailure(.parsingError)
                        return
                    }
                    onSuccess(model)
                case .failure(let error):
                    onFailure(error)
                }
            }
        }
    }
    func getModel(fileName: String, onSuccess: @escaping (Object) -> Void,
                  onFailure: @escaping (FindItError) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            do {
                guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else { return }
                guard let data = try?  String(contentsOfFile: path).data(using: .utf8) else { return }
                guard let model = self?.parse(data: data) else {
                    onFailure(.parsingError)
                    return
                }
                onSuccess(model)
            }
        }
    }
}
