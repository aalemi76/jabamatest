//
//  APIManager.swift
//  FindIt
//
//  Created by Catalina on 9/10/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
enum Route: String {
    case baseURL = "https://api.petfinder.com/v2/"
    case authorize = "oauth2/token"
    case animals = "animals"
    static func generate(_ route: Route) -> String {
        return Route.baseURL.rawValue + route.rawValue
    }
}
