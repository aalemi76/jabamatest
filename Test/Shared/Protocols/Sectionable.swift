//
//  Sectionable.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UICollectionView
protocol Sectionable: class {
    init(cells: [Reusable], headerView: ReusableView?, footerView: ReusableView?)
    func getCells() -> [Reusable]
    func getHeaderView() -> ReusableView?
    func getFooterView() -> ReusableView?
    func append(_ cells: [Reusable])
}
