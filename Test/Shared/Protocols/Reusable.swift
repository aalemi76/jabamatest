//
//  Reusable.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol Reusable {
    init(reuseID: String, cellClass: AnyClass, model: Any)
    func getReuseID() -> String
    func getCellClass() -> AnyClass
    func getModel() -> Any
    func cellDidLoad(_ cell: Updatable)
}
extension Reusable {
    func cellDidLoad(_ cell: Updatable) {
        return
    }
}
