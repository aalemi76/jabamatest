//
//  Viewable.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol Viewable: class {
    func show(result: Result<Any, FindItError>)
}
