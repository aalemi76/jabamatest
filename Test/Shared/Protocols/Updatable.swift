//
//  Updatable.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol Updatable: class {
    func attach(viewModel: Reusable)
    func update(model: Any)
}
extension Updatable {
    func attach(viewModel: Reusable) {
        return
    }
}
