//
//  CollectionViewProvider.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UICollectionView
protocol CollectionViewProvider: class  {
    var sections: [Sectionable] { get set }
    var collectionView: UICollectionView { get }
    var dataSourceHandler: CollectionViewDataSourceHandler { get set }
    var delegateHandler: CollectionViewDelegateHandler { get set }
    init(collectionView: UICollectionView)
    func registerSections(_ sections: [Sectionable])
    func load(_ sections: [Sectionable], dataSourceHandler: CollectionViewDataSourceHandler?, delegateHandler: CollectionViewDelegateHandler?)
    func reloadItems(at indexPaths: [IndexPath])
    func reloadSections(sections: IndexSet)
    func loadNextPage(_ items: [Reusable], from indexPath: IndexPath)
}
extension CollectionViewProvider {
    func registerViews(section: Sectionable) {
        if let view = section.getHeaderView() {
            collectionView.register(view.getViewClass(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: view.getReuseID())
        } else if let view = section.getFooterView() {
            collectionView.register(view.getViewClass(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: view.getReuseID())
        }
    }
    func registerSections(_ sections: [Sectionable]) {
        sections.forEach { (section) in
            section.getCells().forEach { (reusable) in
                let nibCell = UINib(nibName: reusable.getReuseID(), bundle: nil)
                collectionView.register(nibCell, forCellWithReuseIdentifier: reusable.getReuseID())
            }
            registerViews(section: section)
        }
    }
    func load(_ sections: [Sectionable], dataSourceHandler: CollectionViewDataSourceHandler?, delegateHandler: CollectionViewDelegateHandler?) {
        registerSections(sections)
        self.sections = sections
        if let dataSource = dataSourceHandler {
            collectionView.dataSource = dataSource
            collectionView.prefetchDataSource = dataSource
        } else {
            self.dataSourceHandler =  CollectionViewDataSourceHandler(sections: sections)
            collectionView.dataSource = self.dataSourceHandler
            collectionView.prefetchDataSource = self.dataSourceHandler
        }
        if let delegate = delegateHandler {
            collectionView.delegate = delegate
        } else {
            self.delegateHandler = CollectionViewDelegateHandler(sections: sections)
            collectionView.delegate = self.delegateHandler
        }
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    func reloadItems(at indexPaths: [IndexPath]) {
        collectionView.reloadItems(at: indexPaths)
    }
    func reloadSections(sections: IndexSet) {
        collectionView.reloadSections(sections)
    }
    func loadNextPage(_ items: [Reusable], from indexPath: IndexPath) {
        let lastSection = indexPath.section
        let lastRow = indexPath.row + 1
        let indexPaths = items.enumerated().map({ IndexPath(row: lastRow + $0.offset, section: lastSection) })
        sections[lastSection].append(items)
        collectionView.insertItems(at: indexPaths)
    }
}
