//
//  Pageable.swift
//  FindIt
//
//  Created by a.alami on 19/10/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
protocol Pageable: AnyObject {
    func showNextPage(result: Result<Any, FindItError>)
}
