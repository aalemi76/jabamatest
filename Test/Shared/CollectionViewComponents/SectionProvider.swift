//
//  SectionProvider.swift
//  FindIt
//
//  Created by Catalina on 9/18/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
class SectionProvider: Sectionable {
    private var cells: [Reusable]
    private var headerView: ReusableView?
    private var footerView: ReusableView?
    required init(cells: [Reusable], headerView: ReusableView?, footerView: ReusableView?) {
        self.cells = cells
        self.headerView = headerView
        self.footerView = footerView
    }
    func getCells() -> [Reusable] { return cells }
    func getHeaderView() -> ReusableView? { return headerView }
    func getFooterView() -> ReusableView? { return footerView }
    func append(_ cells: [Reusable]) {
        self.cells += cells
    }
}
