//
//  CollectionViewDelegateHandler.swift
//  FindIt
//
//  Created by Catalina on 10/16/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UICollectionView
import Combine
class CollectionViewDelegateHandler: NSObject, UICollectionViewDelegate {
    var sections: [Sectionable]
    private var passSelectedItem = PassthroughSubject<Any, Never>()
    private var passDisplayedItem = PassthroughSubject<Any, Never>()
    func getDisplayedItem() -> PassthroughSubject<Any, Never> { return passDisplayedItem}
    func getSelectedItem() -> PassthroughSubject<Any, Never> { return passSelectedItem }
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let item = sections[indexPath.section].getCells()[indexPath.row].getModel()
        passSelectedItem.send(item)
    }
}
