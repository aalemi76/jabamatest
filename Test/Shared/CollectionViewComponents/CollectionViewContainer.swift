//
//  CollectionViewContainer.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
class CollectionViewContainer: UIView, CollectionViewProvider {
    var sections: [Sectionable] = []
    var collectionView: UICollectionView
    var dataSourceHandler: CollectionViewDataSourceHandler
    var delegateHandler: CollectionViewDelegateHandler
    required init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        dataSourceHandler = CollectionViewDataSourceHandler(sections: sections)
        delegateHandler = CollectionViewDelegateHandler(sections: sections)
        super.init(frame: .zero)
        addSubview(self.collectionView)
        self.collectionView.pinToEdge(self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
