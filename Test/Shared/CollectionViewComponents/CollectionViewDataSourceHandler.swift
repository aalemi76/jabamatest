//
//  CollectionViewDataSourceHandler.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import Combine
class CollectionViewDataSourceHandler: NSObject, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching {
    var sections: [Sectionable]
    private var loadNextPage = PassthroughSubject<IndexPath, Never>()
    func getNextPage() -> PassthroughSubject<IndexPath, Never> { return loadNextPage }
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let section = sections[indexPath.section]
        if let headerView = section.getHeaderView() {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerView.getReuseID(), for: indexPath)
        } else if let footerView = section.getFooterView() {
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerView.getReuseID(), for: indexPath)
        }
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections[section].getCells().count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = sections[indexPath.section].getCells()[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.getReuseID(), for: indexPath)
        (cell as? Updatable)?.attach(viewModel: item)
        (cell as? Updatable)?.update(model: item.getModel())
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard let indexPath = indexPaths.last, indexPath.section < sections.count else { return }
        let section = sections[indexPath.section]
        guard indexPath.row < section.getCells().count && indexPath.row == section.getCells().count - 1 else { return }
        loadNextPage.send(indexPath)
    }
}
