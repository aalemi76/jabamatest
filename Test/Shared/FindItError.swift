//
//  FindItError.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import Foundation
enum FindItError: String, Error {
    case authorize = "Unable to authenticate user. An error occurred during authorization, please check your connection and try again."
    case serverResponse = "Unable to communicate with server, please check your connection and try again."
    case invalidCredentials = "The input credentials are invalid, please double check the required fields and try again."
    case expiredToken = "Your token has been expired please sign in again."
    case errorOnLoading = "An error occured while loading posts."
    case parsingError = "Unable to parse received data to the specified model."
    case pathError = "File wat not founded"
}
