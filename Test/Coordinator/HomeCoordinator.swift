//
//  HomeCoordinator.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UINavigationController
class HomeCoordinator: Coordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator]
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    func start() {
        let vc = HomeViewController(viewModel: HomeViewModel(interactor: Interactor<Mapper>()))
        let tabbarItem = UITabBarItem(title: "", image: UIImage(systemName: "house"), tag: 0)
        tabbarItem.selectedImage = UIImage(systemName: "house.fill")
        vc.tabBarItem = tabbarItem
        vc.navigationItem.title = "Home"
        vc.delegate = self
        navigationController.setNavigationBarHidden(false, animated: false)
        push(viewController: vc, animated: true)
    }
}
extension HomeCoordinator: HomeViewControllerDelegate {
    func didTapItem(with model: Animal) {
        let vc = DetailViewController(viewModel: DetailViewModel(animal: model, interactor: Interactor<Mapper>()))
        vc.navigationItem.title = model.name
        navigationController.setNavigationBarHidden(false, animated: false)
        push(viewController: vc, animated: true)
    }
}
