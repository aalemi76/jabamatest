//
//  LoginCoordinator.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UINavigationController
class LoginCoordinator: Coordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator]
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    func start() {
        let interactor = Interactor<LoginModel>()
        let viewModel = LoginViewModel(interactor: interactor)
        let vc = LoginViewController(viewModel: viewModel)
        vc.delegate = self
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        push(viewController: vc, animated: true)
    }
}
extension LoginCoordinator: LoginViewControllerDelegate {
    func loginButtonTouchUpInside() {
        let vc = MainTabViewController()
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .fullScreen
        present(viewController: vc, animated: true)
    }
}
