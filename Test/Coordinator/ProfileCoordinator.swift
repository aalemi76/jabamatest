//
//  ProfileCoordinator.swift
//  FindIt
//
//  Created by Catalina on 9/11/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit.UINavigationController
class ProfileCoordinator: Coordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator]
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    func start() {
        let vc = ProfileViewController()
        let tabbarItem = UITabBarItem(title: "", image: UIImage(systemName: "person"), tag: 0)
        tabbarItem.selectedImage = UIImage(systemName: "person.fill")
        vc.tabBarItem = tabbarItem
        vc.navigationItem.title = "My Profile"
        navigationController.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: true)
    }
}
