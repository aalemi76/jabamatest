//
//  ListViewCoordinator.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit.UINavigationController
class ListViewCoordinator: Coordinator {
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator]
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    func start() {
        let interactor = Interactor<[CityModel]>()
        let viewModel = ListViewModel(interactor: interactor)
        let vc = ListViewController(viewModel: viewModel)
        vc.delegate = self
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        push(viewController: vc, animated: true)
    }
}
extension ListViewCoordinator: ListViewControllerDelegate {
    func mapButtonDidTap(models: [CityModel], with city: CityModel?) {
        let interactor = Interactor<[CityModel]>()
        let viewModel = MapViewModel(interactor: interactor)
        viewModel.cities = models
        let vc = MapViewController(viewModel: viewModel)
        vc.defaultCity = city
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        push(viewController: vc, animated: true)
    }
}
