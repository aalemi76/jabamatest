//
//  SceneDelegate.swift
//  Test
//
//  Created by Macvps on 8/13/21.
//

import UIKit
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var coordinator: Coordinator?
    var applicationCoordinator: ApplicationCoordinator?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        applicationCoordinator = ApplicationCoordinator(window: UIWindow(frame: windowScene.coordinateSpace.bounds))
        coordinator = applicationCoordinator?.handleUserState(windowScene: windowScene)
        coordinator?.start()
    }
}

