//
//  UIView+ConstraintExt.swift
//  FindIt
//
//  Created by a.alami on 08/09/2020.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
extension UIView {
    func pinToCenterX(_ superview: UIView, upperview: UIView?, centerXConstant: CGFloat = 0, upConstant: CGFloat, widthSize: CGFloat, heightSize: CGFloat, isWidthRelative: Bool = false, isHeightRelative: Bool = false) {
        translatesAutoresizingMaskIntoConstraints = false
        let upConstraint = upperview == nil ? topAnchor.constraint(equalTo: superview.topAnchor, constant: upConstant) : topAnchor.constraint(equalTo: upperview!.bottomAnchor, constant: upConstant)
        let widthConstraint = isWidthRelative ? widthAnchor.constraint(equalTo: superview.widthAnchor, constant: -widthSize) : widthAnchor.constraint(equalToConstant: widthSize)
        let heightConstraint = isHeightRelative ? heightAnchor.constraint(equalTo: superview.heightAnchor, constant: -heightSize) : heightAnchor.constraint(equalToConstant: heightSize)
        NSLayoutConstraint.activate([
            upConstraint,
            centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: centerXConstant),
            widthConstraint,
            heightConstraint])
    }
    func pinToCenter(_ superview: UIView, centerXConstant: CGFloat = 0, centerYConstant: CGFloat = 0, widthSize: CGFloat, heightSize: CGFloat, isWidthRelative: Bool = false, isHeightRelative: Bool = false) {
        translatesAutoresizingMaskIntoConstraints = false
        superview.addSubview(self)
        let widthConstraint = isWidthRelative ? widthAnchor.constraint(equalTo: superview.widthAnchor, constant: -widthSize) : widthAnchor.constraint(equalToConstant: widthSize)
        let heightConstraint = isHeightRelative ? heightAnchor.constraint(equalTo: superview.heightAnchor, constant: -heightSize) : heightAnchor.constraint(equalToConstant: heightSize)
        NSLayoutConstraint.activate([
            centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: centerYConstant),
            centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: centerXConstant),
            widthConstraint,
            heightConstraint])
    }
    func pinToEdge(_ superview: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.topAnchor),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor)])
    }
    @available (iOS 11, *)
    func pinToSafeAreaEdge(_ superview: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor)])
    }
}
