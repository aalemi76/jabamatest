//
//  UIimageView+Ext.swift
//  FindIt
//
//  Created by Catalina on 9/12/20.
//  Copyright © 2020 Deep Minds. All rights reserved.
//

import UIKit
import SDWebImage
extension UIImageView {
    func downloadImage(fromURL url: String) {
        guard let url = URL(string: url) else { return }
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.sd_setImage(with: url, placeholderImage: nil, options: [.continueInBackground, .progressiveLoad], context: nil)
        }
    }
}
